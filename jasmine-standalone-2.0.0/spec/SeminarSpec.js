/**
 * Created by instancetype on 6/14/14.
 */

var SeminarFactory = { create: function(overwrites) {
  var defaults = { name: 'JS-Basics', price: 500, taxFree: false }
    , values = extend(defaults, overwrites)

  return Seminar.create(values.name, values.price, values.taxFree)
}}

function extend(obj, props) {
  var prop
  for (prop in props) obj[prop] = props[prop]

  return obj
}


describe('A Seminar', function() {

  it('should have a name', function() {
    var seminar = SeminarFactory.create({ name: 'JS-Basics' })
    expect(seminar.name()).toEqual('JS-Basics')
  })

  it('should have a price', function() {
    var seminar = SeminarFactory.create({ price: 499.99 })
    expect(seminar.netPrice()).toEqual(499.99)
  })

  it('should have a gross price that adds the VAT to the net price', function() {
    var seminar = SeminarFactory.create({ price: 100 })
    expect(seminar.grossPrice()).toEqual(120)
  })
})

describe('A Tax-free Seminar', function() {
  var seminar
  beforeEach(function() {
    seminar = SeminarFactory.create({ taxFree: true })
  })

  it('should be tax-free', function() {
    expect(seminar).toBeTaxFree()
  })

  it('should have a gross price that matches the net price', function() {
    expect(seminar.grossPrice()).toEqual(seminar.netPrice())
  })
})

describe('A 3-Letter Seminar', function() {
  var seminar
  beforeEach(function() {
    seminar = SeminarFactory.create({ name: 'BDD' })
  })

  it('should have a 3-letter discount applied', function() {
    expect(seminar).toHave3LetterDiscountApplied()
  })

  it('should have a discount of 5%', function() {
    expect(seminar._discount.percentage()).toEqual(5)
  })

  describe('that is priced $200', function() {
    beforeEach(function() {
      seminar = SeminarFactory.create({ name: 'BDD', price: 200 })
    })

    it('should have a discount of $10', function() {
      expect(seminar._discount.value()).toEqual(10)
    })

    it('should have a net price of $190', function() {
      expect(seminar.netPrice()).toEqual(190)
    })
  })
})

describe('A Seminar with a name of more than 3 letters', function() {
  var seminar
  beforeEach(function() {
    seminar = SeminarFactory.create({ name: '1234' })
  })

  it('should not have a 3-letter discount applied', function() {
    expect(seminar).not.toHave3LetterDiscountApplied()
  })

  it('should have a discount of 0%', function() {
    expect(seminar._discount.percentage()).toEqual(0)
  })
})

