beforeEach(function () {
  jasmine.addMatchers({
    toBeTaxFree: function () {
      return {
        compare: function(actual, expected) {
          return {
            pass: actual.isTaxFree()
          }
        }
      }
    }
    ,
    toHave3LetterDiscountApplied: function () {
      return {
        compare: function(actual, expected) {
          return { pass: actual._discount.applied()
                 , message: "Expected " + actual + " to "
                 + "return 'true' on #_discount.applied but got '"
                 + (actual._discount.applied && actual._discount.applied()) + "'"
          }
        }
        ,
        negativeCompare: function(actual, expected) {
          return { pass: !actual._discount.applied()
                 , message: "Expected " + actual + " to "
                 + "return 'false' on #have3LetterDiscountApplied but got '"
                 + (actual._discount.applied && actual._discount.applied()) + "'"
          }
        }
      }
    }
  });
});
