/**
 * Created by instancetype on 6/14/14.
 */
describe('A Cart with several different products', function() {
  var cart
  beforeEach(function() {
    cart = Cart.create()
  })

  it('should have a #grossPriceSum of the contained products', function() {
    var license = Product.create('UltraIDE License', 100)
      , studyBook = StudyBook.create('BDD with Jasmine', 10)
    cart.add(license)
    cart.add(studyBook)
    expect(cart.grossPriceSum()).toEqual(100 * 1.20 + 10 * 1.07)
  })
})


// Organized by methods - Generally not the best approach
describe('Cart', function() {
  var cart
  beforeEach(function() {
    cart = Cart.create()
  })

  describe('.create', function() {
    it('should create a cart that contains 0 products', function() {
      var newCart = Cart.create()
      expect(newCart.numProducts()).toEqual(0)
    })
  })

  describe('#add', function() {
    it('should add a product to the cart', function() {
      var product = {}
      cart.add(product)
      expect(cart.doesContain(product)).toBeTruthy()
    })

    it('should deduct the product from stock', function() {
      var product = { name: 'coolProduct' }
      spyOn(Stock, 'removeProduct')
      cart.add(product)
      expect(Stock.removeProduct).toHaveBeenCalledWith('coolProduct')

    })
  })

  describe('#doesContain', function() {
    it('should return false for a product that is not contained', function() {
      var anotherProduct = {}
      expect(cart.doesContain(anotherProduct)).toBeFalsy()
    })
  })

  describe('#grossPriceSum', function() {
    it('should be 0 for an empty cart', function() {
      expect(cart.grossPriceSum()).toEqual(0)
    })

    it('should return the grossPrice of a single product in a the cart', function() {
      var product = { grossPrice: function() { return 10 } }
      cart.add(product)
      expect(cart.grossPriceSum()).toEqual(10)
    })

    it('should return the sum of two products in the cart', function() {
      var product1 = { grossPrice: function() { return 10 }}
        , product2 = { grossPrice: function() { return 5 }}
      cart.add(product1)
      cart.add(product2)
      expect(cart.grossPriceSum()).toEqual(15)
    })
  })
})

describe('A Product', function() {
  it('should calculate its #grossPrice by adding a VAT of 20%', function() {
    var product = Product.create('Some product', 100)
    expect(product.grossPrice()).toEqual(120)
  })
})