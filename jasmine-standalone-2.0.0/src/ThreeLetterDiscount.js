/**
 * Created by instancetype on 6/14/14.
 */
var ThreeLetterDiscount = { forSeminar: function(seminar) {
                              return Object.create(this).init(seminar)
                          }

                          , init: function(seminar) {
                              this._seminar = seminar
                              return this
                          }

                          , applied: function() { return this._seminar.name().length <= 3 }
                          , percentage: function() { return this.applied() ? 5 : 0 }
                          , value: function() { return this._seminar.originalPrice() * (this.percentage() / 100) }
                          }

