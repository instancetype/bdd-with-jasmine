/**
 * Created by instancetype on 6/14/14.
 */
var Cart = { create: function() {
               return Object.create(Cart).init()
           }
           , init: function() {
               this._items = []
               return this
           }
           , numProducts: function() { return this._items.length }
           , doesContain: function(item) { return this._items.indexOf(item) !== -1 }
           , add: function(item) {
               Stock.removeProduct(item.name);
               this._items.push(item)
           }
           , grossPriceSum: function() {
               return this._items
                 .map(function(item) { return item.grossPrice() })
                 .reduce(function(sum, next) {return sum + next}, 0)
           }
           }

var Product = { _VAT_RATE: 1.20
              , create: function(name, price) {
                  return Object.create(this).init(name, price)
              }
              , init: function(name, price) {
                  this._name = name
                  this._price = price
                  return this
              }
              , grossPrice: function() {
                  return this._price * this._VAT_RATE
              }
              }

var StudyBook = Object.create(Product, { _VAT_RATE: {value: 1.07}})

var Stock = { removeProduct: function() {} }